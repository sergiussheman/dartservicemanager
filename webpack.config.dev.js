const merge = require('webpack-merge');
const webpack = require('webpack');
const webpackConfig = require('./webpack.config');

module.exports = merge(webpackConfig, {

    mode: "development",
    devtool: 'cheap-eval-source-map',
    devServer: {
        watchContentBase: true
    },
    output: {
        pathinfo: true,
        publicPath: '/',
        filename: '[name].js'
    },
    module: {
        rules: [
            {
                use: 'babel-loader',
                test: /\.js$/,
                exclude: /node_modules/
            },
            {
                test: /\.(css|sass|scss)$/,
                use: [
                    'style-loader',
                    'css-loader',
                    'sass-loader'
                ]
            },
            // Loader for the image files
            {
                test: /\.(png|jpg|gif|svg)$/,
                loader: 'file-loader',
                options: {
                    name: 'img/[name].[ext]'
                }
            },
            {
                test: /\.woff2?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                use: 'url-loader?limit=10000'
            },
            {
                test: /\.(ttf|eot|svg)(\?[\s\S]+)?$/,
                use: 'file-loader'
            },
            {
                test: /\.html$/,
                loader: 'html-loader'
            },
            // font-awesome
            {
                test: /font-awesome\.config\.js/,
                use: [{ loader: 'style-loader' }, { loader: 'font-awesome-loader' }]
            }
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            IS_DEV: process.env.NODE_ENV === 'dev'
        })
    ]
});
